﻿using System;
using System.Windows.Forms;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;

namespace prvi
{          
    public partial class Form1 : Form       
    {      
        public double A = 0;      
        public double B = 0;      
        
        public ScriptEngine pyEngine = null;
        public ScriptScope pyScope = null;
        public Form1()
        {
            InitializeComponent();
             pyEngine = Python.CreateEngine();                
             pyScope = pyEngine.CreateScope();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }       
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            A = Convert.ToDouble(textBox1.Text);               
            B = Convert.ToDouble(textBox2.Text);              
            textBox3.Text= Convert.ToString(A + B);                                                                         
        }
        private void subToolStripMenuItem_Click(object sender, EventArgs e)
        {
             A = Convert.ToDouble(textBox1.Text);
            B = Convert.ToDouble(textBox2.Text);
            textBox3.Text = Convert.ToString(A - B);
        }
        private void mulToolStripMenuItem_Click(object sender, EventArgs e)
        {
             A = Convert.ToDouble(textBox1.Text);
            B = Convert.ToDouble(textBox2.Text);
            textBox3.Text = Convert.ToString(A * B);
        }
        private void divToolStripMenuItem_Click(object sender, EventArgs e)
        {
             A = Convert.ToDouble(textBox1.Text);
            B = Convert.ToDouble(textBox2.Text);
            textBox3.Text = Convert.ToString(A / B);
        }
        

        private void loadExtensionToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)       
            {
                ScriptSource skripta = pyEngine.CreateScriptSourceFromFile(openFileDialog1.FileName);
                skripta.Execute(pyScope);
                string Name = pyScope.GetVariable<string>("name");
                dynamic LoadExtension = pyScope.GetVariable("Dodaj");
                LoadExtension(this);

            }

        }

        
    }
}
